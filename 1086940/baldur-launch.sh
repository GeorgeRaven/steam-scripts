#!/usr/bin/env bash

# @Author: George Onoufriou <archer>
# @Date:   2021-10-30T20:20:12+01:00
# @Last modified by:   archer
# @Last modified time: 2021-10-30T20:26:55+01:00

export STEAM_COMPAT_DATA_PATH=$HOME/.steam/steam/steamapps/compatdata/1086940

export STEAM_COMPAT_CLIENT_INSTALL_PATH=”$HOME/.steam/steam”

export WINEDLLOVERRIDES=”cchromaeditorlibrary64=”

cd $HOME/.local/share/Steam/steamapps/common/Proton\ -\ Experimental/

./proton run $HOME/.local/share/Steam/steamapps/common/Baldurs\ Gate\ 3/bin/bg3.exe
